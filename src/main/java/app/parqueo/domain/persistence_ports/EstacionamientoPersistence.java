package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;

public interface EstacionamientoPersistence {
	List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idZona);
	
	Integer validarEstacionamientoExiste(Integer idEstacionamiento);
	
	Estacionamiento seleccionarEstacionamiento(Integer idEstacionamiento);
}
