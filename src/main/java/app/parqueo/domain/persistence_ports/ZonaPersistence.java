package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.ResponseZonaPorSupervisor;
import app.parqueo.domain.model.Zona;

public interface ZonaPersistence {
	
	List<ResponseZona> listarZonas(String coordenadaActual, Integer rangoMetros);
	
	Integer validarZonaExiste(Integer idZona);
	
	Zona seleccionarZona(Integer idZona);
	
	List<ResponseZonaPorSupervisor> listarZonasPorSupervisor(String coordenadaActual, Integer rangoMetros, Integer idSupervisor);
}
