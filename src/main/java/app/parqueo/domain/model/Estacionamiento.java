package app.parqueo.domain.model;

public class Estacionamiento {
	private Integer esta_Id;
	private Integer zona_Id;
	private String esta_Nombre;
	private String esta_Geometry;
	private String esta_Codigo;
	private boolean esta_Disponible;
	private boolean esta_Discapacitado;
	
	public Integer getEsta_Id() {
		return esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		this.esta_Id = esta_Id;
	}
	public Integer getZona_Id() {
		return zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		this.zona_Id = zona_Id;
	}
	public String getEsta_Nombre() {
		return esta_Nombre;
	}
	public void setEsta_Nombre(String esta_Nombre) {
		this.esta_Nombre = esta_Nombre;
	}
	public String getEsta_Geometry() {
		return esta_Geometry;
	}
	public void setEsta_Geometry(String esta_Geometry) {
		this.esta_Geometry = esta_Geometry;
	}
	public String getEsta_Codigo() {
		return esta_Codigo;
	}
	public void setEsta_Codigo(String esta_Codigo) {
		this.esta_Codigo = esta_Codigo;
	}
	public boolean getEsta_Disponible() {
		return esta_Disponible;
	}
	public void setEsta_Disponible(boolean esta_Disponible) {
		this.esta_Disponible = esta_Disponible;
	}
	public boolean getEsta_Discapacitado() {
		return esta_Discapacitado;
	}
	public void setEsta_Discapacitado(boolean esta_Discapacitado) {
		this.esta_Discapacitado = esta_Discapacitado;
	}	
	
}
