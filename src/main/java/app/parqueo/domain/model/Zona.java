package app.parqueo.domain.model;


public class Zona {
	private Integer zona_Id;
	private String zona_Descripcion;
	private Integer muni_Id;
	private boolean zona_Activo;
	private String zona_Geometry;
	
	public int getZona_Id() {
		return zona_Id;
	}
	public void setZona_Id(int zona_Id) {
		this.zona_Id = zona_Id;
	}
	public String getZona_Descripcion() {
		return zona_Descripcion;
	}
	public void setZona_Descripcion(String zona_Descripcion) {
		this.zona_Descripcion = zona_Descripcion;
	}
	public int getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(int muni_Id) {
		this.muni_Id = muni_Id;
	}
	public boolean getZona_Activo() {
		return zona_Activo;
	}
	public void setZona_Activo(boolean zona_Activo) {
		this.zona_Activo = zona_Activo;
	}
	public String getZona_Geometry() {
		return zona_Geometry;
	}
	public void setZona_Geometry(String zona_Geometry) {
		this.zona_Geometry = zona_Geometry;
	}

}
