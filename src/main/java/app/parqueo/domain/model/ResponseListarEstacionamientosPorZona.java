package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarEstacionamientosPorZona extends ResponseError{
	private List<ResponseEstacionamiento> lista;

	public List<ResponseEstacionamiento> getLista() {
		return lista;
	}

	public void setLista(List<ResponseEstacionamiento> lista) {
		this.lista = lista;
	}
}
