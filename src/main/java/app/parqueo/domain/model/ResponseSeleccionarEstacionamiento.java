package app.parqueo.domain.model;

public class ResponseSeleccionarEstacionamiento extends ResponseError{
	private Estacionamiento estacionamiento;

	public Estacionamiento getEstacionamiento() {
		return estacionamiento;
	}

	public void setEstacionamiento(Estacionamiento estacionamiento) {
		this.estacionamiento = estacionamiento;
	}
}
