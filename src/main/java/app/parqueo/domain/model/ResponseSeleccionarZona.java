package app.parqueo.domain.model;

public class ResponseSeleccionarZona extends ResponseError{
	private Zona zona;

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}
}
