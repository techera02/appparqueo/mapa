package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarZonas extends ResponseError{
	
	private List<ResponseZona> lista;

	public List<ResponseZona> getLista() {
		return lista;
	}

	public void setLista(List<ResponseZona> lista) {
		this.lista = lista;
	}
}
