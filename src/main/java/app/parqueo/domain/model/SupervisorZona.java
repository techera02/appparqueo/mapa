package app.parqueo.domain.model;

public class SupervisorZona {
	private Integer szon_Id;
	private Integer supe_Id;
	private Integer zona_Id;
	
	public Integer getSzon_Id() {
		return szon_Id;
	}
	public void setSzon_Id(Integer szon_Id) {
		this.szon_Id = szon_Id;
	}
	public Integer getSupe_Id() {
		return supe_Id;
	}
	public void setSupe_Id(Integer supe_Id) {
		this.supe_Id = supe_Id;
	}
	public Integer getZona_Id() {
		return zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		this.zona_Id = zona_Id;
	}
}
