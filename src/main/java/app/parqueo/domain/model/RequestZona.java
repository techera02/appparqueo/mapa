package app.parqueo.domain.model;

public class RequestZona {

	private String coordenadaActual;
	private Integer rangoMetros;
	
	public String getCoordenadaActual() {
		return coordenadaActual;
	}
	public void setCoordenadaActual(String coordenadaActual) {
		this.coordenadaActual = coordenadaActual;
	}
	public Integer getRangoMetros() {
		return rangoMetros;
	}
	public void setRangoMetros(Integer rangoMetros) {
		this.rangoMetros = rangoMetros;
	}
}
