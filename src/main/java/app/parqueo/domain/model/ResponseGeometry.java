package app.parqueo.domain.model;

public class ResponseGeometry {

	private String longitudA;
	private String latitudA;
	private String longitudB;
	private String latitudB;
	private String longitudC;
	private String latitudC;
	private String longitudD;
	private String latitudD;
	private String longitudA1;
	private String latitudA1;
	
	public String getLongitudA() {
		return longitudA;
	}
	public void setLongitudA(String longitudA) {
		this.longitudA = longitudA;
	}
	public String getLatitudA() {
		return latitudA;
	}
	public void setLatitudA(String latitudA) {
		this.latitudA = latitudA;
	}
	public String getLongitudB() {
		return longitudB;
	}
	public void setLongitudB(String longitudB) {
		this.longitudB = longitudB;
	}
	public String getLatitudB() {
		return latitudB;
	}
	public void setLatitudB(String latitudB) {
		this.latitudB = latitudB;
	}
	public String getLongitudC() {
		return longitudC;
	}
	public void setLongitudC(String longitudC) {
		this.longitudC = longitudC;
	}
	public String getLatitudC() {
		return latitudC;
	}
	public void setLatitudC(String latitudC) {
		this.latitudC = latitudC;
	}
	public String getLongitudD() {
		return longitudD;
	}
	public void setLongitudD(String longitudD) {
		this.longitudD = longitudD;
	}
	public String getLatitudD() {
		return latitudD;
	}
	public void setLatitudD(String latitudD) {
		this.latitudD = latitudD;
	}
	public String getLongitudA1() {
		return longitudA1;
	}
	public void setLongitudA1(String longitudA1) {
		this.longitudA1 = longitudA1;
	}
	public String getLatitudA1() {
		return latitudA1;
	}
	public void setLatitudA1(String latitudA1) {
		this.latitudA1 = latitudA1;
	}
}
