package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarZonasPorSupervisor extends ResponseError{
	private List<ResponseZonaPorSupervisor> lista;

	public List<ResponseZonaPorSupervisor> getLista() {
		return lista;
	}

	public void setLista(List<ResponseZonaPorSupervisor> lista) {
		this.lista = lista;
	}
}
