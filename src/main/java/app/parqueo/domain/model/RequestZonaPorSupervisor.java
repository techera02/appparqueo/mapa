package app.parqueo.domain.model;

public class RequestZonaPorSupervisor {

	private String coordenadaActual;
	private Integer rangoMetros;
	private Integer supe_Id;
	
	public String getCoordenadaActual() {
		return coordenadaActual;
	}
	public void setCoordenadaActual(String coordenadaActual) {
		this.coordenadaActual = coordenadaActual;
	}
	public Integer getRangoMetros() {
		return rangoMetros;
	}
	public void setRangoMetros(Integer rangoMetros) {
		this.rangoMetros = rangoMetros;
	}
	public Integer getSupe_Id() {
		return supe_Id;
	}
	public void setSupe_Id(Integer supe_Id) {
		this.supe_Id = supe_Id;
	}
}
