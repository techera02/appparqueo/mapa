package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.domain.model.ResponseListarZonas;
import app.parqueo.domain.model.RequestZona;
import app.parqueo.domain.model.RequestZonaPorSupervisor;
import app.parqueo.domain.model.ResponseListarZonasPorSupervisor;
import app.parqueo.domain.model.ResponseSeleccionarZona;
import app.parqueo.application.ZonaService;

@RestController
@RequestMapping("/Zona")
public class ZonaController {
	
	private final ZonaService zonaService;
	
	@Autowired
    public ZonaController(ZonaService zonaService) 
	{
        this.zonaService = zonaService;
    }
	
	@PostMapping("/ListarZonas")
    public ResponseEntity<ResponseListarZonas> listarZonas(@RequestBody RequestZona request)
	{  
		ResponseListarZonas response = new ResponseListarZonas();
		response.setEstado(true);
		
			try {
				response.setLista(zonaService.listarZonas(request.getCoordenadaActual(), request.getRangoMetros()));
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
		
		return ResponseEntity.ok(response);
    }
	
	@GetMapping("/SeleccionarZona/{idZona}")
	public ResponseEntity<ResponseSeleccionarZona> seleccionarZona(@PathVariable("idZona")Integer idZona) {
		ResponseSeleccionarZona response = new ResponseSeleccionarZona();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.zonaService.validarZonaExiste(idZona);
			
			if(temp == 1) {
				response.setZona(this.zonaService.seleccionarZona(idZona));
			}else {
				response.setCodError(1);
				response.setMensaje("Zona no registrada");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/ListarZonasPorSupervisor")
    public ResponseEntity<ResponseListarZonasPorSupervisor> listarZonasPorSupervisor(@RequestBody RequestZonaPorSupervisor request)
	{  
		ResponseListarZonasPorSupervisor response = new ResponseListarZonasPorSupervisor();
		response.setEstado(true);
		
			try {
				response.setLista(zonaService.listarZonasPorSupervisor(request.getCoordenadaActual(), request.getRangoMetros(), 
						request.getSupe_Id()));
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
		
		return ResponseEntity.ok(response);
    }
}
