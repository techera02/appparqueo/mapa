package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.EstacionamientoService;
import app.parqueo.domain.model.ResponseListarEstacionamientosPorZona;
import app.parqueo.domain.model.ResponseSeleccionarEstacionamiento;

@RestController
@RequestMapping("/Estacionamiento")
public class EstacionamientoController {
	
	private final EstacionamientoService estacionamientoService;
	
	@Autowired
    public EstacionamientoController(EstacionamientoService estacionamientoService) 
	{
        this.estacionamientoService = estacionamientoService;
    }
	
	@GetMapping("/ListarEstacionamientosPorZona/{idZona}")
    public ResponseEntity<ResponseListarEstacionamientosPorZona> listarEstacionamientosPorZona
    (@PathVariable("idZona")Integer idZona)
	{  
		ResponseListarEstacionamientosPorZona response = new ResponseListarEstacionamientosPorZona();
		response.setEstado(true);
		
			try {
				response.setLista(estacionamientoService.listarEstacionamientosPorZona(idZona));
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
		
		return ResponseEntity.ok(response);
    }
	
	@GetMapping("/SeleccionarEstacionamiento/{idEstacionamiento}")
	public ResponseEntity<ResponseSeleccionarEstacionamiento> seleccionarEstacionamiento(@PathVariable("idEstacionamiento")Integer idEstacionamiento) {
		ResponseSeleccionarEstacionamiento response = new ResponseSeleccionarEstacionamiento();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.estacionamientoService.validarEstacionamientoExiste(idEstacionamiento);
			
			if(temp == 1) {
				response.setEstacionamiento(this.estacionamientoService.seleccionarEstacionamiento(idEstacionamiento));
			}else {
				response.setCodError(1);
				response.setMensaje("Estacionamiento no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
