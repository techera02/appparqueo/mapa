package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.ResponseZonaPorSupervisor;
import app.parqueo.domain.model.Zona;
import app.parqueo.domain.persistence_ports.ZonaPersistence;
import app.parqueo.infrastructure.postgresql.repositories.ZonaRepository;

@Repository("ZonaPersistence")
public class ZonaPersistencePostgres implements ZonaPersistence {

	private final ZonaRepository zonaRepository;

	@Autowired
	public ZonaPersistencePostgres(ZonaRepository zonaRepository) {
		this.zonaRepository = zonaRepository;
	}

	@Override
	public List<ResponseZona> listarZonas(String coordenadaActual, Integer rangoMetros) {
		List<Object[]> list = this.zonaRepository.listarZonas(coordenadaActual, rangoMetros);
		List<ResponseZona> listaZona = new ArrayList<ResponseZona>();

		for (int x = 0; x < list.size(); x++) {
			ResponseZona entity = new ResponseZona();
			entity.setZona_Id(Integer.parseInt(list.get(x)[0].toString()));
			entity.setZona_Descripcion(list.get(x)[1].toString());
			entity.setMuni_Id(Integer.parseInt(list.get(x)[2].toString()));
			entity.setZona_Activo(Boolean.parseBoolean(list.get(x)[3].toString()));
			entity.setLatitud(list.get(x)[4].toString());
			entity.setLongitud(list.get(x)[5].toString());
			listaZona.add(entity);
		}
		return listaZona;
	}

	@Override
	public Integer validarZonaExiste(Integer idZona) {
		return this.zonaRepository.validarZonaExiste(idZona);
	}

	@Override
	public Zona seleccionarZona(Integer idZona) {
		return this.zonaRepository.seleccionarZona(idZona).toZona();
	}

	@Override
	public List<ResponseZonaPorSupervisor> listarZonasPorSupervisor(String coordenadaActual, Integer rangoMetros,
			Integer idSupervisor) {
		List<Object[]> list = this.zonaRepository.listarZonasPorSupervisor(coordenadaActual, rangoMetros, idSupervisor);
		List<ResponseZonaPorSupervisor> listaZona = new ArrayList<ResponseZonaPorSupervisor>();

		for (int x = 0; x < list.size(); x++) {
			ResponseZonaPorSupervisor entity = new ResponseZonaPorSupervisor();
			entity.setZona_Id(Integer.parseInt(list.get(x)[0].toString()));
			entity.setZona_Descripcion(list.get(x)[1].toString());
			entity.setMuni_Id(Integer.parseInt(list.get(x)[2].toString()));
			entity.setZona_Activo(Boolean.parseBoolean(list.get(x)[3].toString()));
			entity.setLatitud(list.get(x)[4].toString());
			entity.setLongitud(list.get(x)[5].toString());
			listaZona.add(entity);
		}
		return listaZona;
	}

}
