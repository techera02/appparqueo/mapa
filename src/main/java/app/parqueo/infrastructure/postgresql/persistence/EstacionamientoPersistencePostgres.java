package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;
import app.parqueo.domain.persistence_ports.EstacionamientoPersistence;
import app.parqueo.infrastructure.postgresql.repositories.EstacionamientoRepository;

@Repository("EstacionamientoPersistence")
public class EstacionamientoPersistencePostgres implements EstacionamientoPersistence {
	
	private final EstacionamientoRepository estacionamientoRepository;
	
	@Autowired
    public EstacionamientoPersistencePostgres(EstacionamientoRepository estacionamientoRepository) 
    {
        this.estacionamientoRepository = estacionamientoRepository;
    }
	
	@Override
	public List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idZona) {
		List<Object[]> list = this.estacionamientoRepository.listarEstacionamientosPorZona(idZona);
		List<ResponseEstacionamiento> listaEstacionamiento = new ArrayList<ResponseEstacionamiento>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseEstacionamiento entity = new ResponseEstacionamiento();
			entity.setEsta_Id(Integer.parseInt(list.get(x)[0].toString()));
			entity.setZona_Id(Integer.parseInt(list.get(x)[1].toString()));
			entity.setEsta_Nombre(list.get(x)[2].toString());
			entity.setEsta_Codigo(list.get(x)[3].toString());
			entity.setEsta_Disponible(Boolean.parseBoolean(list.get(x)[4].toString()));			
			entity.setLongitudA(list.get(x)[5].toString());
			entity.setLatitudA(list.get(x)[6].toString());
			entity.setLongitudB(list.get(x)[7].toString());
			entity.setLatitudB(list.get(x)[8].toString());
			entity.setLongitudC(list.get(x)[9].toString());
			entity.setLatitudC(list.get(x)[10].toString());
			entity.setLongitudD(list.get(x)[11].toString());
			entity.setLatitudD(list.get(x)[12].toString());
			entity.setLongitudA1(list.get(x)[13].toString());
			entity.setLatitudA1(list.get(x)[14].toString());			
			entity.setPlaca(list.get(x)[15].toString());
			entity.setFechainicio(list.get(x)[16].toString());
			entity.setDuracion(list.get(x)[17].toString());
			entity.setNombrecompleto(list.get(x)[18].toString());
			entity.setEsta_Discapacitado(Boolean.parseBoolean(list.get(x)[19].toString()));		
			
			listaEstacionamiento.add(entity);
		}
		return listaEstacionamiento;
	}
	
	@Override
	public Integer validarEstacionamientoExiste(Integer idEstacionamiento) {
		return this.estacionamientoRepository.validarEstacionamientoExiste(idEstacionamiento);
	}

	
	@Override
	public Estacionamiento seleccionarEstacionamiento(Integer idEstacionamiento) {
		return this.estacionamientoRepository.seleccionarEstacionamiento(idEstacionamiento).toEstacionamiento();
	}
}
