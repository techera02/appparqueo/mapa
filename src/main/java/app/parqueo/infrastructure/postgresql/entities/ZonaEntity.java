package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Zona;

@Entity
@Table(name="\"Zona\"")
public class ZonaEntity {
	@Id
	private int Zona_Id;
	private String Zona_Descripcion;
	private int Muni_Id;
	private boolean Zona_Activo;
	private String Zona_Geometry;
	
	public int getZona_Id() 
	{
		return Zona_Id;
	}
	public void setZona_Id(int zona_Id) 
	{
		Zona_Id = zona_Id;
	}
	public String getZona_Descripcion() {
		return Zona_Descripcion;
	}
	public void setZona_Descripcion(String zona_Descripcion) {
		Zona_Descripcion = zona_Descripcion;
	}
	public int getMuni_Id() {
		return Muni_Id;
	}
	public void setMuni_Id(int muni_Id) {
		Muni_Id = muni_Id;
	}
	public boolean getZona_Activo() {
		return Zona_Activo;
	}
	public void setZona_Activo(boolean zona_Activo) {
		Zona_Activo = zona_Activo;
	}
	public String getZona_Geometry() {
		return Zona_Geometry;
	}
	public void setZona_Geometry(String zona_Geometry) {
		Zona_Geometry = zona_Geometry;
	}
	
	public Zona toZona() {
		Zona zona = new Zona();
        BeanUtils.copyProperties(this, zona);
        return zona;
    }
	
}
