package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.EstacionamientoEntity;

public interface EstacionamientoRepository  extends JpaRepository<EstacionamientoEntity, Integer> {
	@Query(value = "SELECT * FROM \"UFN_ListarEstacionamientosPorZona\"(:idZona)", nativeQuery = true)
	List<Object[]> listarEstacionamientosPorZona(@Param("idZona") Integer idZona);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarEstacionamientoExiste\"(:idEstacionamiento)", nativeQuery = true)
	Integer validarEstacionamientoExiste(@Param("idEstacionamiento") Integer idEstacionamiento);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarEstacionamiento\"(:idEstacionamiento)", nativeQuery = true)
	EstacionamientoEntity seleccionarEstacionamiento(@Param("idEstacionamiento") Integer idEstacionamiento);
}
