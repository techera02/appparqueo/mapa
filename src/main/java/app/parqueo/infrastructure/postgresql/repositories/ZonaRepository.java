package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ZonaEntity;

public interface ZonaRepository extends JpaRepository<ZonaEntity, Integer> {
	@Query(value = "SELECT * FROM \"UFN_ListarZonasExistenEstacionamientos\"(:coordenadaActual,:rangoMetros)", nativeQuery = true)
	List<Object[]> listarZonas(@Param("coordenadaActual") String coordenadaActual,
			@Param("rangoMetros") Integer rangoMetros);

	@Query(value = "SELECT * FROM \"UFN_ValidarZonaExiste\"(:idZona)", nativeQuery = true)
	Integer validarZonaExiste(@Param("idZona") Integer idZona);

	@Query(value = "SELECT * FROM \"UFN_SeleccionarZona\"(:idZona)", nativeQuery = true)
	ZonaEntity seleccionarZona(@Param("idZona") Integer idZona);

	@Query(value = "SELECT * FROM \"UFN_ListarZonasPorSupervisor\"(:coordenadaActual,:rangoMetros,:idSupervisor)", nativeQuery = true)
	List<Object[]> listarZonasPorSupervisor(@Param("coordenadaActual") String coordenadaActual,
			@Param("rangoMetros") Integer rangoMetros, @Param("idSupervisor") Integer idSupervisor);
}