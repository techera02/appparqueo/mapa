package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;
import app.parqueo.domain.persistence_ports.EstacionamientoPersistence;

@Service
public class EstacionamientoService {
	
	EstacionamientoPersistence estacionamientoPersistence;
	
	public EstacionamientoService(EstacionamientoPersistence estacionamientoPersistence)
	{
		this.estacionamientoPersistence = estacionamientoPersistence;
	}
	
	public List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idZona)
	{
        return this.estacionamientoPersistence.listarEstacionamientosPorZona(idZona);
    }
	
	public Integer validarEstacionamientoExiste(Integer idEstacionamiento) {
		return this.estacionamientoPersistence.validarEstacionamientoExiste(idEstacionamiento);
	}
	
	public Estacionamiento seleccionarEstacionamiento(Integer idEstacionamiento) {
		return this.estacionamientoPersistence.seleccionarEstacionamiento(idEstacionamiento);
	}
}
