package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.ResponseZonaPorSupervisor;
import app.parqueo.domain.model.Zona;
import app.parqueo.domain.persistence_ports.ZonaPersistence;

@Service
public class ZonaService {

	ZonaPersistence zonaPersistence;

	public ZonaService(ZonaPersistence zonaPersistence) {
		this.zonaPersistence = zonaPersistence;
	}

	public List<ResponseZona> listarZonas(String coordenadaActual, Integer rangoMetros) {
		return this.zonaPersistence.listarZonas(coordenadaActual, rangoMetros);
	}

	public Integer validarZonaExiste(Integer idZona) {
		return this.zonaPersistence.validarZonaExiste(idZona);
	}

	public Zona seleccionarZona(Integer idZona) {
		return this.zonaPersistence.seleccionarZona(idZona);
	}

	public List<ResponseZonaPorSupervisor> listarZonasPorSupervisor(String coordenadaActual, Integer rangoMetros,
			Integer idSupervisor) {
		return this.zonaPersistence.listarZonasPorSupervisor(coordenadaActual, rangoMetros, idSupervisor);
	}

}
